Style Node Title

This module adds a class to the title field for content types. This module will be very usefull when you want to design different style for specific nodes of single content type.

Instructions for use:

Pre-requisite, if using nested fieldsets group using htmlelement

1) Add the field "field_style_title" as a checkbox to your content type and place with the title for easy editor use.
2) On the content check the field_style_title to add class to the title
